*** Settings ***
Documentation     platform settings for platform demo (freeswitch)
...

Library 	syprunner.robot_plugin.Pilot


*** Variables ***
${platform_name}=	demo

@{all_call_formats}=  ext


*** Keywords ***


Unit get platform configuration
	get platform configuration


