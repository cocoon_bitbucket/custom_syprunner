*** Settings ***
Documentation     platform settings for platform btelu
...

Library 	syprunner.robot_plugin.Pilot


*** Variables ***
${platform_name}=	btic

@{all_call_formats}=  sid_ext  national  international  universal


*** Keywords ***


Unit CallFeatureAccessCode
    [Arguments]     ${userA}	${FAC}
    Open Session    ${userA}

    call_feature_access_code    ${userA}   	${FAC}
    wait_call_confirmed    ${userA}
    log    'wait for AS to disconnect'
    # sleep 1
    wait_call_disconnected   ${userA}
    Close Session


Unit AB_REGISTER_A_CALL_B_PLAY_BYE
    [Arguments]     ${userA}    ${userB}    ${format}
    Open Session    ${userA}    ${userB}

    call_user    ${userA}    ${userB}	${format}
    wait_incoming_call    ${userB}
    answer_call    ${userB}    200
    wait_call_confirmed    ${userA}
    # sleep 1
    hangup    ${userA}
    Close Session

